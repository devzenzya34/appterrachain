import * as DropdownMenu from "@radix-ui/react-dropdown-menu";
import { ConnectType, useWallet } from "@terra-money/wallet-provider";

const ConnectionNameDict: Partial<Record<ConnectType, string>> = {
	[ConnectType.EXTENSION]: "Extension",
	[ConnectType.WALLETCONNECT]: "Wallet Connect",
}

export function ConnectWalletButton () {

	const { availableConnectTypes, connect } = useWallet();
	const allowedConnection = availableConnectTypes.filter((c) => [ConnectType.WALLETCONNECT].includes(c)) //ConnectType.EXTENSION,
	return(
		<DropdownMenu.Root>
			<DropdownMenu.Trigger>
				Connect Wallet
			</DropdownMenu.Trigger>

			<DropdownMenu.Content>
				{ allowedConnection.map(
					(connection) => (
						<DropdownMenu.Item onClick={() => connect(connection)} key={connection}>
							{  ConnectionNameDict[connection ]}
						</DropdownMenu.Item>
					)
				)}
			</DropdownMenu.Content>
		</DropdownMenu.Root>
	)
}
