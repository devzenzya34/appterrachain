import { useWallet } from "@terra-money/wallet-provider";
import * as DropdownMenu from "@radix-ui/react-dropdown-menu";

export function WalletButton() {

    const { disconnect, wallets } = useWallet()
    return (
        <DropdownMenu.Root>
            <DropdownMenu.Trigger>
                { wallets[0].terraAddress }
            </DropdownMenu.Trigger>
            <DropdownMenu.Content>
                        <DropdownMenu.Item onClick={() => disconnect()}>
                            Disconnect
                        </DropdownMenu.Item>

            </DropdownMenu.Content>
        </DropdownMenu.Root>
    )
}