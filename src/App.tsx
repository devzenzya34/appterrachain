import { useChainOptions } from "@terra-money/wallet-provider";
import { AppProviders } from "./configuration/app";
import { Route, Routes } from "react-router-dom";
import { Home } from "./pages/Home";
import { Header } from "./components/layout/Header";

export function App() {

    const chainOptions = useChainOptions()

    return (
        chainOptions && (
            <AppProviders { ...chainOptions }>
                <Header />
                <Routes>
                    <Route index element={ <Home/> }/>
                </Routes>
            </AppProviders>
        )
    )
}