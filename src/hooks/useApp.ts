import { useContext } from 'react';
import { OrneContext } from '../contexts/OrneProvider';

export function useApp() {
    return useContext(OrneContext);
}