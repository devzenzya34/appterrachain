import { useApp } from "./useApp";
import { useConnectedWallet } from "@terra-money/wallet-provider";
import { useTerraNativeBalancesQuery } from "../client/queries/nativeBalance";
import { EMPTY_NATIVE_BALANCES } from "types"

export function useTerraNativeBalances() {

    const { queryClient } = useApp()
    const connectedWallet = useConnectedWallet()

    const { data = EMPTY_NATIVE_BALANCES } = useTerraNativeBalancesQuery(connectedWallet!.walletAddress, queryClient)
    return data
}